* ０サイズのファイル

```
ls -ltrh | grep ".png" | find . -type f -size 0 | tail
```

* 0サイズのtail

```
ls -lthS | grep ".png" | tail
```

* 進行状況

```
ls -lthr | grep ".png" | tail
```

* 0サイズのファイルの数

```
ls -ltrh | grep ".png" | find . -type f -size 0 | wc -l
```

* 変換したファイルの数

```
ls -l | grep ".png" | wc -l
```
